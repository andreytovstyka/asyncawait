﻿// <copyright file="AsyncAwait.cs" company="Company">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsyncAwait
{
    /// <summary>
    /// Demonstration class of task "Async/Await".
    /// </summary>
    public static class AsyncAwait
    {
        private static readonly object ConsoleLock = new ();
        private static readonly Random Random = new ();

        /// <summary>
        /// Demonstration Async/Await task.
        /// </summary>
        /// <param name="arrayLenght">array length.</param>
        /// <param name="minRandomValue">minimum random value.</param>
        /// <param name="maxRandomValue">maximum random value.</param>
        /// <exception cref="ArgumentException">if the maxRandomValue less than minRandomValue or array lenght less than one.</exception>
        /// <returns>average value of the antecedent array of integers.</returns>
        public static async Task<double> DemonstrationAsync(int arrayLenght, int minRandomValue, int maxRandomValue)
        {
            List<Task> consoleTasks = new () { };

            Helpers.IfValueLessThanDefinedException(value: arrayLenght, edgeValue: 1, nameof(arrayLenght));
            Helpers.IfValueLessThanDefinedException(value: maxRandomValue, edgeValue: minRandomValue, nameof(maxRandomValue));

            var taskCreateArray = CreateIntArrayWithRandomElementsAsync(arrayLenght, minRandomValue, maxRandomValue);
            var taskRandomNumber = GetRandomIntegerAsync(minRandomValue, maxRandomValue);

            await Task.WhenAll(taskCreateArray, taskRandomNumber);
            var createdArray = await taskCreateArray;
            var randomNumber = await taskRandomNumber;

            var taskMultiplyArrayByNumber = MultiplyArrayByNumberAsync(createdArray, randomNumber);
            consoleTasks.Add(ConsoleMessageAsync($"Random number {randomNumber} generated."));
            consoleTasks.Add(ConsoleArrayAsync(createdArray, "Created array."));

            var multipliedArrayByNumber = await taskMultiplyArrayByNumber;

            var taskCalculateAverage = GetTheAverageOfTheElementsOfIntegerArrayAsync(multipliedArrayByNumber);
            var taskSortArray = SortArrayByAscendingAsync(multipliedArrayByNumber);
            consoleTasks.Add(ConsoleArrayAsync(multipliedArrayByNumber, "Multiplied array."));

            List<Task> tasks = new () { taskSortArray, taskCalculateAverage };
            while (tasks.Count > 0)
            {
                var finishedTask = await Task.WhenAny(tasks);
                if (finishedTask == taskSortArray)
                {
                    var sortedArray = await taskSortArray;
                    consoleTasks.Add(ConsoleArrayAsync(sortedArray, "Ordered array."));
                }

                if (finishedTask == taskCalculateAverage)
                {
                    var calculatedAverage = await taskCalculateAverage;
                    consoleTasks.Add(ConsoleMessageAsync($"Average value {calculatedAverage}"));
                }

                tasks.Remove(finishedTask);
            }

            await Task.WhenAll(consoleTasks);

            return taskCalculateAverage.Result;
        }

        /// <summary>
        /// Create int array with random elements.
        /// </summary>
        /// <param name="arrayLenght">array length.</param>
        /// <param name="minRandomValue">minimum random value.</param>
        /// <param name="maxRandomValue">maximum random value.</param>
        /// <exception cref="ArgumentException">if the maxRandomValue less than minRandomValue or array lenght less than one.</exception>
        /// <returns>integer array.</returns>
        public static async Task<int[]> CreateIntArrayWithRandomElementsAsync(int arrayLenght, int minRandomValue, int maxRandomValue)
        {
            Helpers.IfValueLessThanDefinedException(value: arrayLenght, edgeValue: 1, nameof(arrayLenght));
            Helpers.IfValueLessThanDefinedException(value: maxRandomValue, edgeValue: minRandomValue, nameof(maxRandomValue));

            int[] array = new int[arrayLenght];

            for (int i = 0; i < arrayLenght; i++)
            {
                array[i] = await GetRandomIntegerAsync(minRandomValue, maxRandomValue);
            }

            return array;
        }

        /// <summary>
        /// multiplies the array by a number.
        /// </summary>
        /// <param name="array">input Array.</param>
        /// <param name="number">number.</param>
        /// <exception cref="ArgumentNullException">If array is null.</exception>
        /// <exception cref="ArgumentException">If array is empty.</exception>
        /// <returns>Multiplied array by number.</returns>
        public static async Task<int[]> MultiplyArrayByNumberAsync(int[] array, int number)
        {
            return await Task.Run(() =>
           {
               Helpers.IfArrayIsNullOrEmptyException(array);

               var result = array.Select(i => i * number).ToArray();

               return result;
           });
        }

        /// <summary>
        /// Generate a random integer value.
        /// </summary>
        /// <param name="minRandomValue">minimum random value.</param>
        /// <param name="maxRandomValue">maximum random value.</param>
        /// <exception cref="ArgumentException">if the maxRandomValue less than minRandomValue.</exception>
        /// <returns>random integer value.</returns>
        public static async Task<int> GetRandomIntegerAsync(int minRandomValue, int maxRandomValue)
        {
            return await Task.Run(() =>
            {
                Helpers.IfValueLessThanDefinedException(value: maxRandomValue, edgeValue: minRandomValue, nameof(maxRandomValue));

                lock (Random)
                {
                    return Random.Next(minRandomValue, maxRandomValue + 1);
                }
            });
        }

        /// <summary>
        /// Sort input int array by ascending.
        /// </summary>
        /// <param name="array">input array.</param>
        /// <exception cref="ArgumentNullException">If array is null.</exception>
        /// <exception cref="ArgumentException">If array is empty.</exception>
        /// <returns>Sort array by ascending.</returns>
        public static async Task<int[]> SortArrayByAscendingAsync(int[] array)
        {
            return await Task.Run(() =>
            {
                Helpers.IfArrayIsNullOrEmptyException(array);

                var result = array.OrderBy(x => x).ToArray();
                return result;
            });
        }

        /// <summary>
        /// Calculate the average of the elements of an integer array.
        /// </summary>
        /// <param name="array">input array.</param>
        /// <exception cref="ArgumentNullException">If array is null.</exception>
        /// <exception cref="ArgumentException">If array is empty.</exception>
        /// <returns>average of the elements.</returns>
        public static async Task<double> GetTheAverageOfTheElementsOfIntegerArrayAsync(int[] array)
        {
            return await Task.Run(() =>
            {
                Helpers.IfArrayIsNullOrEmptyException(array);

                var result = array.Average();
                return result;
            });
        }

        /// <summary>
        /// Print array to console.
        /// </summary>
        /// <typeparam name="T">type or array.</typeparam>
        /// <param name="array">array.</param>
        /// <param name="title">title of array.</param>
        /// <exception cref="ArgumentNullException">If title or array is null.</exception>
        /// <exception cref="ArgumentException">If title or array is empty.</exception>
        /// <returns><see cref="Task"/> representing the asynchronous operation ConsoleArray.</returns>
        public static async Task ConsoleArrayAsync<T>(T[] array, string title)
        {
            await Task.Run(() =>
            {
                Helpers.IfStringNullOrEmptyException(title);
                Helpers.IfArrayIsNullOrEmptyException(array);

                lock (ConsoleLock)
                {
                    Console.WriteLine($"\n{title}");
                    for (int i = 0; i < array.Length; i++)
                    {
                        Console.WriteLine($"\t{array[i]}");
                    }
                }
            });
        }

        /// <summary>
        /// Print message.
        /// </summary>
        /// <param name="message">message.</param>
        /// <exception cref="ArgumentNullException">If string is null.</exception>
        /// <exception cref="ArgumentException">If string is empty.</exception>
        /// <returns><see cref="Task"/> representing the asynchronous operation ConsoleMessage.</returns>
        public static async Task ConsoleMessageAsync(string message)
        {
            await Task.Run(() =>
            {
                Helpers.IfStringNullOrEmptyException(message);

                lock (ConsoleLock)
                {
                    Console.WriteLine($"\n{message}");
                }
            });
        }
    }
}
