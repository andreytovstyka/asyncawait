﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsyncAwait
{
    /// <summary>
    /// Class programm.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Entry point.
        /// </summary>
        /// <returns><<see cref="Task"/>Representing the asynchronous operation.</returns>
        internal static async Task Main()
        {
            try
            {
                var result = await AsyncAwait.DemonstrationAsync(arrayLenght: 10, minRandomValue: 1, maxRandomValue: 10);
                Console.WriteLine(string.Format("\nresult: {0}", result));
            }
            catch (AggregateException ex)
            {
                foreach (var e in ex.Flatten().InnerExceptions)
                {
                    Console.WriteLine(string.Format("\n{0}\t{1}", e.GetType().Name, e.Message));
                }
            }
        }
    }
}