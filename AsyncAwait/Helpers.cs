﻿// <copyright file="Helpers.cs" company="Company">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsyncAwait
{
    /// <summary>
    /// Helpers.
    /// </summary>
    internal static class Helpers
    {
        /// <summary>
        /// Throw an appropriate exception if the string is null or empty.
        /// </summary>
        /// <param name="someString">some string.</param>
        /// <exception cref="ArgumentNullException">If string is null.</exception>
        /// <exception cref="ArgumentException">If string is empty.</exception>
        public static void IfStringNullOrEmptyException(string someString)
        {
            if (someString == null)
            {
                throw new ArgumentNullException(nameof(someString));
            }

            if (string.IsNullOrWhiteSpace(someString))
            {
                throw new ArgumentException($"String {nameof(someString)} is empty.");
            }
        }

        /// <summary>
        /// Throw an appropriate exception if the array is null or empty.
        /// </summary>
        /// <typeparam name="T">type of array.</typeparam>
        /// <param name="array">array.</param>
        /// <exception cref="ArgumentNullException">If array is null.</exception>
        /// <exception cref="ArgumentException">If array is empty.</exception>
        public static void IfArrayIsNullOrEmptyException<T>(T[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException($"Array {nameof(array)} is empty.");
            }
        }

        /// <summary>
        /// Throw an exception if the value less than edge value.
        /// </summary>
        /// <param name="value">value.</param>
        /// <param name="edgeValue">edge value.</param>
        /// <param name="paramName">parameter name.</param>
        /// <exception cref="ArgumentException">if the value less than edgeValue.</exception>
        public static void IfValueLessThanDefinedException(int value, int edgeValue, string paramName)
        {
            if (value < edgeValue)
            {
                throw new ArgumentException($"The {paramName} must be greater than {edgeValue}, but was {value}");
            }
        }
    }
}
