// <copyright file="AsyncAwaitTests.cs" company="Company">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace AsyncAwait.Tests
{
    using NUnit.Framework;

    /// <summary>
    /// Async/Await Tests.
    /// </summary>
    [TestFixture]
    public class AsyncAwaitTests
    {
        private const int MultiplyByTen = 10;
        private static readonly int[] EmptyArray = Array.Empty<int>();
        private static readonly int[] ArrayFromTenToOne = new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };

        /// <summary>
        /// Checking if an exception will be thrown if an empty array is passed.
        /// </summary>
        [Test]
        public void MultiplyArrayByNumberAsync_EmptyArray_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await AsyncAwait.MultiplyArrayByNumberAsync(EmptyArray, MultiplyByTen));
        }

        /// <summary>
        /// Checking if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void MultiplyArrayByNumberAsync_NullArray_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await AsyncAwait.MultiplyArrayByNumberAsync(null, MultiplyByTen));
        }

        /// <summary>
        /// Checking the result of MultipliesArrayByNumber method.
        /// </summary>
        /// <returns><see cref="Task"/>representing the asynchronous unit test.</returns>
        [Test]
        public async Task MultiplyArrayByNumberAsync_Array_ReturnsArray()
        {
            var expected = new int[] { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 };

            var act = await AsyncAwait.MultiplyArrayByNumberAsync(ArrayFromTenToOne, MultiplyByTen);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Checking if an exception will be thrown if an empty array is passed.
        /// </summary>
        [Test]
        public void SortArrayByAscendingAsync_EmptyArray_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await AsyncAwait.SortArrayByAscendingAsync(EmptyArray));
        }

        /// <summary>
        /// Checking if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void SortArrayByAscendingAsync_NullArray_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await AsyncAwait.SortArrayByAscendingAsync(null));
        }

        /// <summary>
        /// Checking the result of SortArrayByAscending method.
        /// </summary>
        /// <returns><see cref="Task"/>representing the asynchronous unit test.</returns>
        [Test]
        public async Task SortArrayByAscendingAsync_Array_ReturnsArray()
        {
            var expected = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            var act = await AsyncAwait.SortArrayByAscendingAsync(ArrayFromTenToOne);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Checking if an exception will be thrown if an empty array is passed.
        /// </summary>
        [Test]
        public void GetTheAverageOfTheElementsOfIntegerArrayAsync_EmptyArray_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await AsyncAwait.GetTheAverageOfTheElementsOfIntegerArrayAsync(EmptyArray));
        }

        /// <summary>
        /// Checking if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void GetTheAverageOfTheElementsOfIntegerArrayAsync_NullArray_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await AsyncAwait.GetTheAverageOfTheElementsOfIntegerArrayAsync(null));
        }

        /// <summary>
        /// Checking the result of GetTheAverageOfTheElementsOfIntegerArray method.
        /// </summary>
        /// <returns><see cref="Task"/>representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetTheAverageOfTheElementsOfIntegerArrayAsync_Array_ReturnsArray()
        {
            var expected = 5.5;

            var act = await AsyncAwait.GetTheAverageOfTheElementsOfIntegerArrayAsync(ArrayFromTenToOne);

            Assert.That(act, Is.EqualTo(expected));
        }
    }
}